
-- DEPENDENCIES
local STATEMANAGER = require 'util.statemanager'
local SIGNALS      = require 'util.signal'

return function (scenario_name, scene_name)
  SIGNALS.send("endDialogueBox")
  STATEMANAGER.load("Scenario", scenario_name, scene_name)
  while true do coroutine.yield() end
end

