
local SIGNALS  = require 'util.signal'
local OVERLAY = require 'common.overlay'
local CO      = require 'util.coroutine'

local TIME = 0.75 -- seconds

return function (dir)
  local done = false
  SIGNALS.send("endDialogueBox")
  OVERLAY.panIn(dir, TIME, function() done = true end)
  while not done do CO.yield() end
end
