
-- DEPENDENCIES
local SIGNALS = require 'util.signal'
local CO      = require 'util.coroutine'

return function (time)
  local done = false
  CO.createWaiter(time * 1000, function () done = true end)
  SIGNALS.send("endDialogueBox")
  while not done do CO.yield() end
end

