
-- DEPENDENCIES
local SIGNALS  = require 'util.signal'
local FLAGS    = require 'game.profile.flags'

return function ()
  SIGNALS.sendNextFrame("checkAutoPlay")
  coroutine.yield("DONE")
end
