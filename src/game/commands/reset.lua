
-- DEPENDENCIES
local STATEMANAGER = require 'util.statemanager'
local SIGNALS      = require 'util.signal'
local FLAGS        = require 'game.profile.flags'

-- MODULE
return function ()
  local scenario_name = "Ty's House"

  FLAGS.deleteTemp()
  SIGNALS.send("endDialogueBox")

  if FLAGS.check("perm.cycles <= 4") then
    scenario_name = "The Goddesses"
  end

  STATEMANAGER.load("Scenario", scenario_name)
  coroutine.yield("DONE")
end

