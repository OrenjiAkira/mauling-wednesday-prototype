
-- DEPENDENCIES
local SIGNALS  = require 'util.signal'

return function (scene_name)
  local _scene = scene_name

  print("jump", _scene)
  SIGNALS.send("playScene", _scene)
  coroutine.yield("DONE")
end

