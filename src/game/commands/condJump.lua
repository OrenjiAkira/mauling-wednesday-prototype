
-- DEPENDENCIES
local SIGNALS  = require 'util.signal'
local FLAGS    = require 'game.profile.flags'

return function (condition, scene_name)
  local _condition = condition
  local _scene_name = scene_name
  local willJump = FLAGS.check(_condition)

  print(_condition, willJump)
  print("condJump", _scene_name)

  if willJump then
    SIGNALS.send("playScene", _scene_name)
    coroutine.yield("DONE")
  end
end

