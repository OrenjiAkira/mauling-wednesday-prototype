
-- DEPENDENCIES
local SCREEN    = require 'screen'
local FONTS     = require 'fonts'
local STATES    = require 'lux.pack' 'game.gamestates'
local Drawable  = require 'common.drawable'
local Decorator = require 'common.decorator'


-- MODULES
local GameState = require 'common.gamestate'
local View = GameState:new()

function View:instance (obj)
  GameState:inherit(obj)

  -- PRIVATE STUFF
  local _state = STATES.PickScenario
  local _selected
  local _scenario_title
  local _drawable_text


  -- PUBLIC METHODS
  function obj.onLoad ()
    local graphics = love.graphics
    local w, h = love.window.getMode()
    local font = FONTS.setFont("roboto_regular", 2)

    -- load values
    _selected = _state.getSelected()
    _scenario_title = graphics.newText(font, _selected)

    -- add drawables
    _drawable_text = Decorator(
      Drawable(_scenario_title, {32, h / 2, }),
      function ()
        graphics.setColor(255, 255, 255, 255)
      end
    )

    -- send to screen
    obj.focus()
  end

  function obj.onQuit ()
    obj.rest()
  end

  function obj.onFocus ()
    SCREEN.addDrawable(_drawable_text, 1)
  end

  function obj.onRest ()
    SCREEN.removeDrawable(_drawable_text, 1)
  end

  function obj.onUpdate (dt)
    local current_selection = _state.getSelected()
    if _selected ~= current_selection then
      _selected = current_selection
      _scenario_title:set(_selected)
    end
  end

end

return View()
