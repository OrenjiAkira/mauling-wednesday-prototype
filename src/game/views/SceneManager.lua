
-- MODULE
local GameState = require 'common.gamestate'
local View = GameState:new()

function View:instance (obj)
  GameState:inherit(obj)
end

return View()
