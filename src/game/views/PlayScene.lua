
-- DEPENDENCIES
local GameState    = require 'common.gamestate'

-- MODULES
local View = GameState:new()

function View:instance (obj)
  GameState:inherit(obj)
end

return View()
