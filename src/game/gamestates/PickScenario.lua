
-- DEPENDENCIES
local SOUND        = require 'sound'

local CO           = require 'util.coroutine'
local CONTROLS     = require 'util.controls'
local STATEMANAGER = require 'util.statemanager'
local PARSER       = require 'util.parser'

local LIST         = require 'common.list'


-- MODULE
local GameState = require 'common.gamestate'
local State = GameState:new()

function State:instance (obj)
  GameState:inherit(obj)
  -- PRIVATE STUFF
  local _scenarios
  local _selected
  local _count
  local _move_prev
  local _move_next
  local _delay = 100


  -- ACTION MAPPING
  local function selectConfirm ()
    local scenario_filepath = _scenarios[_selected]:match("(.+)[.]md")
    local parsed_scenario = PARSER(scenario_filepath)
    SOUND.playSFX("yes")
    STATEMANAGER.load("SceneList", parsed_scenario)
  end

  local function selectCancel ()
    SOUND.playSFX("no")
    CONTROLS.setMap()
    love.event.quit()
  end

  local function moveCursorPrev ()
    SOUND.playSFX("cursor")
    _selected = LIST.previous(_selected, _count)
  end

  local function moveCursorNext ()
    SOUND.playSFX("cursor")
    _selected = LIST.next(_selected, _count)
  end


  -- PUBLIC METHODS
  function obj.onLoad (...)
    -- load values
    local select_prev = CO.doAndDelay(moveCursorPrev, _delay)
    local select_next = CO.doAndDelay(moveCursorNext, _delay)
    _scenarios = love.filesystem.getDirectoryItems("game/script")
    _selected = next(_scenarios)
    _count = #_scenarios

    -- load action mapping
    CONTROLS.setMap(
      {
        PRESS_A     = selectConfirm,
        PRESS_B     = selectCancel,
        PRESS_UP    = select_prev,
        PRESS_LEFT  = select_prev,
        PRESS_DOWN  = select_next,
        PRESS_RIGHT = select_next,
      }
    )
  end

  function obj.onUpdate (dt)
  end

  function obj.onQuit ()
  end

  -- GETTERS
  function obj.getSelected ()
    local s = tostring(_selected) .. " " .. _scenarios[_selected]
    return s:match("(.+)[.]md") or s
  end
end

-- RETURN
return State()
