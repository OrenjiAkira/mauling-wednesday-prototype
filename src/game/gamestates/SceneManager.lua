
-- DEPENDENCIES
local SIGNALS      = require 'util.signal'
local STATEMANAGER = require 'util.statemanager'
local PARSER       = require 'util.parser'
local FLAGS        = require 'game.profile.flags'

-- MODULE
local GameState = require 'common.gamestate'
local State = GameState:new()

function State:instance (obj)
  GameState:inherit(obj)
  local _scenario_name
  local _scenario
  local _scenes
  local _finish

  local function on_playScene (scene_name)
    print(">> SCENE: ", scene_name)
    STATEMANAGER.push("PlayScene", _scenes[scene_name])
    FLAGS.setPlayed(_scenario_name, scene_name)
  end

  local function on_changeScene (scene_name)
    print(">> SCENE: ", scene_name)
    STATEMANAGER.pop()
    STATEMANAGER.push("PlayScene", _scenes[scene_name])
    FLAGS.setPlayed(_scenario_name, scene_name)
  end

  function obj.onLoad (scenario_name, scene_name)
    _scenario_name = scenario_name
    _scenes = PARSER(scenario_name)
    SIGNALS.addListener("changeScene", on_changeScene)
    SIGNALS.addListener("playScene", on_playScene)
    SIGNALS.sendNextFrame("playScene", scene_name)
  end

  function obj.onFocus ()
    _finish = (function ()
      local count = 0
      return function ()
        count = count + 1
        if count >= 2 then
          STATEMANAGER.pop()
        end
      end
    end)()
  end

  function obj.onUpdate (dt)
    if _finish then _finish() end
  end

  function obj.onQuit ()
    SIGNALS.removeListener("changeScene", on_changeScene)
  end
end

return State()
