
-- DEPENDENCIES
local SOUND        = require 'SOUND'
local SIGNALS      = require 'util.signal'
local CONTROLS     = require 'util.controls'
local STATEMANAGER = require 'util.statemanager'
local CO           = require 'util.coroutine'
local FLAGS        = require 'game.profile.flags'
local CONTEXTMENU  = require 'common.menu'
local OVERLAY      = require 'common.overlay'
local Activity     = require 'common.activity'

-- HELPERS
local unpack = table.unpack or unpack

-- CONSTS
local W, H = love.window.getMode()
local M = 16
local DELAY = 1000
local __NOTHING__ = function () end

-- MODULE
local Menu = Activity:new()

function Menu:instance (obj)
  Activity:inherit(obj)
  local _scenelist
  local _scenarios
  local _context
  local _scroll
  local _map
  local _delaying
  local _wait_for_start

  local function holdOn (time, handle, ...)
    local start = 0
    local params = { ... }
    return function (dt)
      start = start + dt
      if start >= time then
        (handle or __NOTHING__)(unpack(params))
        _delaying = false
      end
    end
  end

  local function interact (scene_name)
    CONTROLS.setMap()
    SIGNALS.send("runScenarioScene", scene_name)
  end

  local function move (scenario_name)
    FLAGS.temp.time = FLAGS.temp.time + 15
    _delaying = function (dt) end
    STATEMANAGER.pop()
    OVERLAY.panOut("left", DELAY/1000, function ()
      CO.createWaiter(DELAY/2, function ()
        STATEMANAGER.push("Scenario", scenario_name)
        OVERLAY.panIn("right", DELAY/1000)
      end)
    end)
  end

  local function wait ()
    FLAGS.temp.time = FLAGS.temp.time + 30
    CONTROLS.setMap()
    SIGNALS.send("updateAvailableScenes")
    SIGNALS.send("checkAutoPlay")
  end

  local function confirm ()
    CONTEXTMENU.confirm()
  end

  local function cancel ()
    SOUND.playSFX("no")
    CONTEXTMENU.cancel()
  end

  local function moveUp ()
    SOUND.playSFX("cursor")
    CONTEXTMENU.previous()
  end

  local function moveDown ()
    SOUND.playSFX("cursor")
    CONTEXTMENU.next()
  end

  local function loadControls ()
    _map= {
      PRESS_A = confirm,
      PRESS_UP = moveUp,
      PRESS_DOWN = moveDown,
    }
    CONTROLS.setMap(_map)
  end

  function obj.setAvailable(available_scenes, available_scenarios)
    _scenelist = available_scenes
    _scenarios = available_scenarios
  end

  function obj.onStart(available_scenes, available_scenarios)
    _scenelist = available_scenes
    _scenarios = available_scenarios
    _scroll = false
    _context = "scenario_base"
    _delaying = true
    _wait_for_start = (function ()
      local start = 0
      return function (dt)
        start = start + dt
        if start >= DELAY/1000 then
          loadControls()
          _delaying = false
          _wait_for_start = __NOTHING__
        end
      end
    end)()
  end

  function obj.onUpdate(dt)
    _wait_for_start(dt)
    if not _delaying then
      if CONTEXTMENU.begin(_context, M, H / 3, _scroll) then
        if _context == "scenario_interact" then
          for _, scene_name in ipairs(_scenelist) do
            if CONTEXTMENU.item(scene_name) then
              SOUND.playSFX("yes")
              CONTROLS.setMap()
              _delaying = true
              CO.createWaiter(DELAY, function()
                interact(scene_name)
                _delaying = false
              end)
            end
          end
        elseif _context == "scenario_move" then
          for _, scenario_name in ipairs(_scenarios) do
            if CONTEXTMENU.item(scenario_name) then
              SOUND.playSFX("yes")
              CONTROLS.setMap()
              _delaying = true
              CO.createWaiter(DELAY, function ()
                move(scenario_name)
                _delaying = false
              end)
            end
          end
        else
          -- first option
          if CONTEXTMENU.item("Interact", #_scenelist == 0) then
            if #_scenelist ~= 0 then
              _scroll = 4
              _context = "scenario_interact"
              _map.PRESS_B = cancel
              SOUND.playSFX("yes")
            else
              SOUND.playSFX("no")
            end
          end
          -- second option
          if CONTEXTMENU.item("Move", #_scenarios == 0) then
            if #_scenarios ~= 0 then
              _scroll = 4
              _context = "scenario_move"
              _map.PRESS_B = cancel
              SOUND.playSFX("yes")
            else
              SOUND.playSFX("no")
            end
          end
          -- third option
          if FLAGS.check("perm.prologue_done")
          and CONTEXTMENU.item("Wait (+30min)") then
            SOUND.playSFX("yes")
            CONTROLS.setMap()
            _delaying = true
            CO.createWaiter(DELAY, function ()
              wait()
              _delaying = false
              CONTROLS.setMap(_map)
            end)
          end
        end
        CONTEXTMENU.finish()
      else
        _scroll = false
        _map.PRESS_B = nil
        _context = "scenario_base"
      end
    end
  end

  function obj.onStop()
  end

end

return Menu
