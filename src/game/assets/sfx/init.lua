return {
  yes    = { "yes.wav", 4 },
  no     = { "no.wav", 4 },
  cursor = { "cursor.wav", 8 },
  voice0 = { "voice0.wav", 8 },
  voice1 = { "voice1.wav", 8 },
}
