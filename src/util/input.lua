
-- DEPENDENCIES
local CONTROLS = require 'util.controls'

-- PRIVATE INFO
local _keyMapping = {
  z = "A",
  x = "B",
  c = "C",
  up = "UP",
  right = "RIGHT",
  down = "DOWN",
  left = "LEFT",
  f8 = "QUIT",
  escape = "PAUSE",
}
local _enabledActions = {
  A = true,
  B = true,
  C = true,
  UP = true,
  RIGHT = true,
  DOWN = true,
  LEFT = true,
  QUIT = true,
  PAUSE = true,
}

local function sendAction (atype, aname)
  if not _enabledActions[aname] then return end
  local full_name = atype .. "_" .. aname
  CONTROLS.send(full_name)
end

local function handlePress (key)
  local action_found = _keyMapping[key]
  if not action_found then return end
  sendAction("PRESS", action_found)
end

local function handleRelease (key)
  local action_found = _keyMapping[key]
  if not action_found then return end
  sendAction("RELEASE", action_found)
end

local function handleHold (key)
  local action_found = _keyMapping[key]
  if not action_found then return end
  sendAction("HOLD", action_found)
end

local function checkHeldKeyboardKeys ()
  for key in pairs(_keyMapping) do
    if love.keyboard.isDown(key) then
      handleHold(key)
    end
  end
end


-- MODULE
local Input = {}

-- PUBLIC METHODS
function Input.keyPressed (key)
  handlePress(key)
end

function Input.keyReleased (key)
  handleRelease(key)
end

function Input.load ()
  -- check saved input mapping
  -- load default values
end

function Input.update ()
  -- check held controls
  checkHeldKeyboardKeys()
  CONTROLS.update()
end

return Input
