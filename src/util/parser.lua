
-- HELPERS
local unpack = table.unpack or unpack
local getFileData = love and love.filesystem.newFileData or io.open
local lines = love and love.filesystem.lines or io.lines

-- CONSTS
local SCRIPTDIR = "game/script/"
local EXTENSION = ".scnscript"
local STATES = {
  READING = 0,
  LISTING = 1,
}

local function parseSpeech (rawspeech)
  local speaker = rawspeech:match("%*%*(.-)%*%*")
  local expression = rawspeech:match("%((.-)%)") or "Neutral"
  local line = rawspeech:match("[\"%[].+[\"%]]")
  line = line:gsub("\\n", "\n")
  line = line:gsub("\\t", "\t")
  return { "speech", speaker, expression, line }
end

local function checkFile (filename)
  local filepath = SCRIPTDIR..filename..EXTENSION
  local filedata, err = getFileData(filepath, "r")
  local error_msg = ("No such scenario: %s\n%s"):format(filename, err)
  return assert(filedata, error_msg) and filepath
end

local function cleanName (name)
  return name:lower():gsub("[\':]", ""):gsub("[%s\t]", "_")
end

local function log (...)
  if __VERBOSE__ then
    print(...)
  end
end

return function (scenario)
  local filename = cleanName(scenario)
  local filepath = checkFile(filename)
  -- local file = getFileData(filepath, "r")
  -- local stream = file:read("*a")
  local scene_list = {}
  local neighbours = {}
  local buffer = false
  local state = STATES.READING
  local scene
  local index

  log("> parsing start")
  log("---------------")
  log()
  for line in lines(filepath) do
    if #line > 0 then
      -- get scene name if it's a new scene
      local scene_name = line:match("^%[(.+)%]")

      -- if you are listing buffer stuff, do this
      if state == STATES.LISTING then
        local arg = line:match("^+[\t%s]*(.+)[\t%s]*") if arg then
          arg = arg:gsub("!(%w)", "not %1")
          arg = arg:gsub("&", " and ")
          arg = arg:gsub("||", " or ")
          arg = tonumber(arg) or arg
          log("arg: ", "/"..tostring(arg).."/")
          table.insert(buffer, arg)
        else
          -- finish buffer
          log("buffer done")
          log()
          state = STATES.READING
          buffer = false
        end
      end

      -- if you are done listing buffer stuff then read next line
      if state == STATES.READING then
        -- if it starts with a word, it's a directive
        if line:match("^%w") then
          local directive = line:match("%w+")
          log()
          log("new directive:", directive)
          if directive == "repeatable" then
            -- the scene is repeatable!
            log("scene is repeatable!")
            log()
            scene.repeatable = true
          elseif directive == "trigger" then
            -- the scene needs the following flags to be triggerable
            buffer = scene.flags
            state = STATES.LISTING
          elseif directive == "neighbours" then
            -- the scenenario has the following neighbours
            buffer = {}
            neighbours = buffer
            state = STATES.LISTING
          else
            -- the scene is calling a command
            buffer = { directive }
            scene[index] = buffer
            index = index + 1
            state = STATES.LISTING
          end
        elseif line:match("^%*%*") then
          log("> speech found")
          scene[index] = parseSpeech(line)
          index = index + 1
        end
      end
      if scene_name then
        scene = {
          flags = {}
        }
        scene_list[scene_name] = scene
        index = 1
      end
    end
  end
  log()
  log("---------------")
  log("> parsing done")

  return scene_list, neighbours
end
