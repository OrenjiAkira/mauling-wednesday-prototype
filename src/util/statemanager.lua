
-- DEPENDENCIES
local STATES      = require 'lux.pack' 'game.gamestates'
local VIEWS       = require 'lux.pack' 'game.views'
local Stack       = require 'common.stack'

-- PRIVATE INFO
local _state_stack = Stack()
local _view_stack = Stack()
local _pushit = false

-- MODULE
local StateManager = {}


-- PUBLIC METHODS
function StateManager.load (name, ...)
  while _state_stack.getSize() > 0 do _state_stack.pop().quit() end
  while _view_stack.getSize() > 0 do _view_stack.pop().quit() end
  StateManager.push(name, ...)
end

function StateManager.push (name, ...)
  _pushit = { name, {...} }
end

local function pushIt (name, ...)
  local state = _state_stack.getHead()
  local view = _view_stack.getHead()
  local args = { ... }
  if state then state.rest() end
  if view then view.rest() end
  _state_stack.push(STATES[name])
  _view_stack.push(VIEWS[name])
  state = _state_stack.getHead()
  state.load(unpack(args))
  view = _view_stack.getHead()
  view.load(unpack(args))
end

function StateManager.pop ()
  local state = _state_stack.pop()
  local view = _view_stack.pop()
  if state then state.quit() end
  if view then view.quit() end
  state = _state_stack.getHead()
  view = _view_stack.getHead()
  if state then state.focus() end
  if view then view.focus() end
end

function StateManager.update (dt)
  if _pushit then
    pushIt(_pushit[1], unpack(_pushit[2]))
    _pushit = false
  end
  local state = _state_stack.getHead()
  local view = _view_stack.getHead()
  if state then state.update(dt) end
  if view then view.update(dt) end
end

function StateManager.getState ()
  return _state_stack.getHead()
end

function StateManager.getView ()
  return _view_stack.getHead()
end

return StateManager
