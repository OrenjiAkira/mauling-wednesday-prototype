
-- DEPENDENCIES
local Queue = require 'lux.common.Queue'

-- HELPERS
local unpack = table.unpack or unpack

-- MODULE
local Signal = {}

-- PRIVATE
local _listeners = {}
local _signal_queue = Queue(256)

-- PUBLIC METHODS
function Signal.addListener (label, f)
  assert(label and f, "Cannot add listener with no label or callback.")
  _listeners[label] = _listeners[label] or {}
  _listeners[label][f] = f
  return f
end

function Signal.removeListener (label, f)
  assert(label and f, "Cannot remove listener with no label or callback.")
  if _listeners[label] then
    _listeners[label][f] = nil
  end
end

function Signal.purgeListener (label)
  assert(label and f, "Cannot purge listener with no label.")
  _listeners[label] = nil
end

function Signal.send (label, ...)
  assert(label, "Cannot send signal with no label.")
  if not _listeners[label] then return end
  for f in pairs(_listeners[label]) do
    _listeners[label][f](...)
  end
end

function Signal.sendNextFrame (label, ...)
  assert(label, "Cannot send signal with no label.")
  _signal_queue.push({ label, {...} })
end

function Signal.resolveQueue ()
  while not _signal_queue.isEmpty() do
    local label, args = unpack(_signal_queue.pop())
    Signal.send(label, unpack(args))
  end
end

return Signal
