
-- DEPENDENCIES
local Class     = require 'common.class'

-- FACILITATIONS
local floor    = math.floor
local sort     = table.sort
local insert   = table.insert
local graphics = love.graphics

-- MODULE
local Screen = Class:new()

function Screen:instance(obj)
  local _layers         = { {}, {}, {}, {}, }
  local _ordered_layers = { {}, {}, {}, {}, }
  local _dirty          = { false, false, false, false, }

  local function linearSearch (layer, drawable, e, d)
    e = e or 1
    d = d or #layer
    for i = e, d do
      if layer[i] == drawable then return i end
    end
    return false
  end

  local function findInLayer (layer, drawable, e, d)
    -- this is a binary search for an element
    e = e or 1
    d = d or #layer
    -- if layer empty
    if d < e then return false end
    local m = floor((e + d) / 2)
    -- if drawable is in the mid position return that position
    if layer[m] == drawable then return m end
    -- if drawable is to the right, look for it there
    -- if not, look to the left
    if drawable.z > layer[m].z then
      return findInLayer(layer, drawable, m + 1, d)
    elseif drawable.z < layer[m].z then
      return findInLayer(layer, drawable, e, m - 1)
    else
      linearSearch(layer, drawable, e, d)
    end
  end

  local function cleanOrderedLayer (layerid)
    local layer  = _layers[layerid]
    local olayer = _ordered_layers[layerid]
    for i = #olayer, 1, -1 do
      local drawable = olayer[i]
      if not layer[drawable] then
        local N = #olayer
        olayer[i] = olayer[N]
        olayer[N] = nil
      end
    end
    _dirty[layerid] = true
  end

  local function addNewDrawable (drawable, layerid)
    local olayer = _ordered_layers[layerid]
    if not findInLayer(olayer, drawable) then
      insert(olayer, drawable)
      _dirty[layerid] = true
    end
  end

  local function sortByZAndY (a, b)
    return a.z < b.z or a.z == b.z and a.y < b.y
  end

  -- PUBLIC METHODS
  function obj.clear ()
    for id, layer in ipairs(_layers) do
      local olayer = _ordered_layers[id]
      for drawable in pairs(layer) do layer[drawable] = nil end
      cleanOrderedLayer(id)
    end
  end

  function obj.addDrawable (drawable, layerid)
    local layer = _layers[layerid]
    if layer then
      layer[drawable] = true
      addNewDrawable(drawable, layerid)
    end
  end

  function obj.removeDrawable (drawable, layerid)
    -- if layer was specified, delete only from that layer
    if layerid and _layers[layerid][drawable] then
      _layers[layerid][drawable] = nil
      cleanOrderedLayer(layerid)
      return true
    end
    return false
  end

  function obj.purgeDrawable (drawable)
    -- if layer not specified, delete from all layers
    local success = false
    for id, layer in ipairs(_layers) do
      if layer[drawable] then
        layer[drawable] = nil
        success = true
      end
      cleanOrderedLayer(id)
    end
    return success
  end

  function obj.update ()
    -- order by z index
    for i, layer in ipairs(_layers) do
      local olayer = _ordered_layers[i]
      -- reorder drawables by Z and Y
      if _dirty[i] then
        sort(olayer, sortByZAndY)
        _dirty[i] = false
      end
    end
  end

  function obj.render ()
    graphics.setColor(0xff, 0xff, 0xff, 0xff)
    for _, olayer in ipairs(_ordered_layers) do
      for i, drawable in ipairs(olayer) do
        drawable.draw()
      end
    end
  end

  function obj.__operators:tostring ()
    local s = "\n"
    s = s .. "##### set layers\n"
    for i, layer in ipairs(_layers) do
      s = s .. "SL#" .. tostring(i) .. "\n"
      local c = 0
      for e in pairs(layer) do
        c = c + 1
        s = s .. tostring(c) .. ":\n" .. tostring(e) .. "\n"
      end
    end
    s = s .. "##### ordered layers\n"
    for i, olayer in ipairs(_ordered_layers) do
      s = s .. "OL#" .. tostring(i) .. "\n"
      for z, e in ipairs(olayer) do
        s = s .. tostring(z) .. ":\n" .. tostring(e) .. "\n"
      end
    end
    s = s .. "#####\n\n"
    return s
  end

end

return Screen()
