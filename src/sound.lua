
-- DEPENDENCIES
local Audio      = love.audio
local SoundTrack = require 'util.soundtrack'
local SoundFX    = require 'util.soundfx'


-- HELPERS
local unpack = table.unpack or unpack


-- CONSTS
local SFXPATH = "game/assets/sfx/"
local BGMPATH = "game/assets/bgm/"
local SFXLIST = require(SFXPATH)
local BGMLIST = require(BGMPATH)


-- MODULE
local SoundPlayer = require 'common.class' :new()

function SoundPlayer:instance (obj)
  -- PRIVATE STUFF
  local _sfx = {}
  local _bgm = {}

  -- initialising audio sources
  for label, info in pairs(SFXLIST) do
    local filename, buffer = unpack(info)
    _sfx[label] = SoundFX(SFXPATH .. filename, buffer)
  end

  for label, filename in pairs(BGMLIST) do
    _bgm[label] = SoundTrack(BGMPATH .. filename)
  end

  local function crossFade (bgm)
    -- cross fade here
  end

  function obj.playSFX (name, pitch)
    local sfx = _sfx[name]
    if sfx then
      return sfx.play(pitch)
    end
  end

  function obj.playBGM (name)
    local bgm = _bgm[name]
    if bgm then
      crossFade(bgm)
    end
  end
end

return SoundPlayer()
