
-- DEPENDENCIES
local SCREEN = require 'screen'
local Timer  = require 'hump.timer'
local COLORS = require 'common.colors'

-- HELPERS
local graphics = love.graphics
local delta = love.timer.getDelta
local unpack = table.unpack or unpack

-- CONSTS
local W, H = love.window.getMode()
local function __NOTHING__() end
local DARK = { 0, 0, 0, 0xff }
local TRANSPARENT = { 0, 0, 0, 0 }
local DIR = { left = -W, right = W, }

-- MODULE
local OVERLAY = {}

-- PRIVATE
local _color = { 0, 0, 0, 0 }
local _pos = { 0, 0 }
local _drawable = {
  x = 0,
  y = 0,
  z = 99,
  draw = function ()
    graphics.push()
    graphics.translate(unpack(_pos))
    graphics.setColor(_color)
    graphics.rectangle("fill", 0, 0, W, H)
    graphics.pop()
  end
}

SCREEN.addDrawable(_drawable, 4)

function OVERLAY.darken (s)
  _color = { unpack(TRANSPARENT) }
  Timer.tween(s, _color, DARK, 'in-out-quad')
end

function OVERLAY.lighten (s)
  _color = { unpack(DARK) }
  Timer.tween(s, _color, TRANSPARENT, 'in-out-quad')
end

-- blacken via pan
function OVERLAY.panOut (to, s, lam)
  lam = lam or __NOTHING__
  _color = { unpack(DARK) }
  _pos[1] = DIR[to]
  Timer.tween(s, _pos, { 0, 0 }, 'in-expo',
    function ()
      _pos[1] = 0
      lam()
    end)
end

-- unblacken via pan
function OVERLAY.panIn (to, s, lam)
  lam = lam or __NOTHING__
  _color = { unpack(DARK) }
  _pos[1] = 0
  Timer.tween(s, _pos, { DIR[to], 0 }, 'in-out-cubic',
    function ()
      _color = { unpack(TRANSPARENT) }
      _pos[1] = 0
      lam()
    end)
end

return OVERLAY
