
-- HELPERS
local min = math.min


-- MODULE
local TextBox = require 'common.class' :new()

function TextBox:instance (obj, speaker, expression, line)
  local _progress = 1
  local _speaker = speaker
  local _expression = expression
  local _line = line
  local _linelen = #_line

  function obj.__init ()
  end

  function obj.getSpeaker ()
    return _speaker
  end

  function obj.getExpression ()
    return _expression
  end

  function obj.advance ()
    _progress = min(_progress + 1, _linelen)
  end

  function obj.getText ()
    local substr = _line:sub(1, _progress)
    return substr
  end

  function obj.skip ()
    _progress = _linelen
  end

  function obj.getChar ()
    return obj.getText():sub(-1)
  end

  function obj.isDone ()
    return _progress == _linelen
  end
end

return TextBox
