
-- DEPENDENCIES
local Drawable = require 'common.drawable'


-- HELPERS
local graphics = love.graphics
local unpack   = table.unpack or unpack


-- CONSTS
local DEFAULTS = {
  0, 0,        -- x, y
  100, "left", -- width, alignment
  0,           -- angle
  1, 1,        -- scale
  0, 0,        -- offset
  0, 0,        -- shear
}


-- MODULE
local DrawableText = Drawable:new()

function DrawableText:instance (obj, object, params, zindex)
  Drawable:inherit(obj, object, {}, zindex)
  local _params = params or {}


  -- BUILDING DEFAULTS
  if not _params[1] then _params[1] = DEFAULTS[1] end
  if not _params[2] then _params[2] = DEFAULTS[2] end
  if not _params[3] then _params[3] = DEFAULTS[3] end
  if not _params[4] then _params[4] = DEFAULTS[4] end


  -- PUBLIC METHODS
  function obj.draw ()
    graphics.printf(obj.getObject(), unpack(_params))
  end

  function obj.__operators:index (k)
    if     k == "x"      then return _params[1]
    elseif k == "y"      then return _params[2]
    elseif k == "width"  then return _params[3]
    elseif k == "align"  then return _params[4]
    elseif k == "angle"  then return _params[5]
    elseif k == "sx"     then return _params[6]
    elseif k == "sy"     then return _params[7]
    elseif k == "ox"     then return _params[8]
    elseif k == "oy"     then return _params[9]
    elseif k == "kx"     then return _params[10]
    elseif k == "ky"     then return _params[11]
    elseif k == "object" then return getmetatable(obj)["object"]
    elseif k == "z"      then return _z
    end
    return getmetatable(obj)[k]
  end
end


-- STATIC METHODS
function DrawableText:getParamIndex (name)
  if     name == "x"     then return 1
  elseif name == "y"     then return 2
  elseif name == "width" then return 3
  elseif name == "align" then return 4
  elseif name == "angle" then return 5
  elseif name == "sx"    then return 6
  elseif name == "sy"    then return 7
  elseif name == "ox"    then return 8
  elseif name == "oy"    then return 9
  elseif name == "kx"    then return 10
  elseif name == "ky"    then return 11
  end
end


return DrawableText
