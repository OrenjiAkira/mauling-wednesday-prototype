
local unpack = table.unpack or unpack
local g = love.graphics

local DEFAULTS = {
  0, 0, -- x, y
  0,    -- angle
  1, 1, -- scale
  0, 0, -- offset
  0, 0, -- shear
}
local Z = 0
local OBJECT = g.newMesh({
                           {  0,  0, 0, 0, 0xff, 0xff, 0xff },
                           { 32,  0, 0, 0, 0xff, 0xff, 0xff },
                           { 32, 32, 0, 0, 0xff, 0xff, 0xff },
                           {  0, 32, 0, 0, 0xff, 0xff, 0xff },
                         }, "fan", "dynamic")

-- MODULE
local Drawable = require 'common.class' :new()

function Drawable:instance (obj, object, params, zindex)
  -- PRIVATE
  local _object = object or OBJECT
  local _params = params or {}
  local _z = zindex or Z


  -- BUILDING DEFAULTS
  if not _params[1] then _params[1] = DEFAULTS[1] end
  if not _params[2] then _params[2] = DEFAULTS[2] end


  -- PUBLIC METHODS
  function obj.getObject ()
    return _object
  end

  function obj.setObject (obj)
    _object = obj
  end

  function obj.setParam (i, val)
    _params[i] = val
  end

  function obj.update (dt)
    -- if you have a dynamic spritebatch, you might need to update it
  end

  function obj.draw ()
    g.draw(_object, unpack(_params))
  end

  function obj.__operators:index (k)
    if     k == "x"      then return _params[1]
    elseif k == "y"      then return _params[2]
    elseif k == "angle"  then return _params[3]
    elseif k == "sx"     then return _params[4]
    elseif k == "sy"     then return _params[5]
    elseif k == "ox"     then return _params[6]
    elseif k == "oy"     then return _params[7]
    elseif k == "kx"     then return _params[8]
    elseif k == "ky"     then return _params[9]
    elseif k == "object" then return _object
    elseif k == "z"      then return _z
    end
    return getmetatable(obj)[k]
  end

  function obj.__operators:tostring ()
    return (tostring(_object) .. ":\n" .. table.concat(_params, ", "))
  end
end

-- STATIC METHODS
function Drawable:getParamIndex (name)
  if     name == "x"     then return 1
  elseif name == "y"     then return 2
  elseif name == "angle" then return 3
  elseif name == "sx"    then return 4
  elseif name == "sy"    then return 5
  elseif name == "ox"    then return 6
  elseif name == "oy"    then return 7
  elseif name == "kx"    then return 8
  elseif name == "ky"    then return 9
  end
end

return Drawable
