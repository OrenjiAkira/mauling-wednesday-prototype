
local Drawable = require 'common.drawable'

-- HELPERS
local function __nothing__() end
local graphics = love.graphics

-- MODULE
local Decorator = require 'common.drawable' :new()

function Decorator:instance (obj, object, lambda, zindex, params )
  Drawable:inherit(obj, object, params, zindex)
  -- PRIVATE
  local _decor = lambda or __nothing__

  -- PUBLIC METHODS
  function obj.getObject ()
    return obj.object.getObject()
  end

  function obj.setObject (newobject)
    obj.object.setObject(newobject)
  end

  function obj.setDecor (f)
    _decor = f
  end

  function obj.draw ()
    graphics.push()
    _decor()
    obj.object.draw()
    graphics.pop()
  end
end

return Decorator
