
# Mauling Wednesday
# <small>Speech Box and Scene Model Script prototype</small>

This prototype code is a test for developing a scene and dialog system for a
game project named "Mauling Wednesday".

# Install/Play

To play/test you will need the following packages:

1. make
2. wget
3. [LOVE2D v0.10.2](https://love2d.org/)

They are needed to configure the game.

This means that, currently, there is no easy way to play this on Windows,
only on Unix-based systems (OSX, Linux). Unless you get you hands in Windows
Bash with those above packages.

# Third-party libraries credits

Currently I am using only [dkjson][dkjsonlib] and [hump][humplib] libraries.
Their licenses can be found in their own project pages.

[dkjsonlib]: http://dkolf.de/src/dkjson-lua.fsl/home
[humplib]: https://github.com/vrld/hump

