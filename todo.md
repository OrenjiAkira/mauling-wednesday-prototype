
# To Do List

+ [x] Make gamestate view
+ [x] Make drawables
+ [x] Make gs choose scenario view
+ [x] Make gs choose scene view
+ [x] Make interaction sounds
+ [x] Make sfx playing system
+ [x] Make textbox object
+ [x] Make textbox drawable
+ [x] Make gs scene play
+ [x] Make gs scene play view
+ [x] Make bgm playing system
+ [x] Make scripted commands work
+ [x] Make option boxes mid-dialogue
+ [x] Make actual game scenes
+ [x] Add flags
+ [x] Add time
+ [ ] Implement some more commands
