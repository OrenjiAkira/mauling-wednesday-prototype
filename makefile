# definitions
GAMEDIR=src
LIBDIR=libs
LIBTARGET=${GAMEDIR}/libs

GITHUB=https://github.com
SAVEDIR=~/Library/Application\ Support/LOVE/MaulingWednesday/

LUXURL=${GITHUB}/kazuo256/luxproject.git
HUMPURL=${GITHUB}/vrld/hump.git
LUXREPO=${LIBDIR}/luxproject
HUMPREPO=${LIBDIR}/hump

LUXLIB=${LIBTARGET}/lux
HUMPLIB=${LIBTARGET}/hump
DKJSONLIB=${LIBTARGET}/dkjson.lua

DEPENDENCIES=${LIBDIR} ${LIBTARGET} ${LUXLIB} ${DKJSONLIB} ${HUMPLIB}

.PHONY: all clean purge

# defaults
all: ${DEPENDENCIES}
	love src

clean:
	rm -rf ${LUXLIB}
	rm -rf ${HUMPLIB}

purge:
	rm -rf ${DEPENDENCIES}

save:
	ln -s ${SAVEDIR}/savefile

# dependency hell
${DKJSONLIB}:
	wget -O ${DKJSONLIB} -- http://dkolf.de/src/dkjson-lua.fsl/raw/dkjson.lua?name=16cbc26080996d9da827df42cb0844a25518eeb3

${LIBDIR}:
	mkdir ${LIBDIR}

${LIBTARGET}:
	mkdir ${LIBTARGET}

${LUXLIB}: ${LUXREPO}
	cp -R ${LUXREPO}/lib/lux ${LUXLIB}

${LUXREPO}:
	git clone ${LUXURL} ${LUXREPO}

${HUMPLIB}: ${HUMPREPO}
	cp -R ${HUMPREPO} ${HUMPLIB}

${HUMPREPO}:
	git clone ${HUMPURL} ${HUMPREPO}

# code generation
gamestate:
	echo "do some shit here"
